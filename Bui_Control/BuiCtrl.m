function controller = BuiCtrl(model, CtrlParam)

if nargin == 0
   buildingType = 'Infrax';  
   ModelOrders.range = [100, 200, 600]; % add any reduced order model you wish to have
   ModelOrders.choice = 200;            % insert model order or 'full' for full order SSM 
   ModelOrders.off_free = 0;            %  augmented model
   reload = 0;
%    construct the model
   model = BuiModel(buildingType, ModelOrders, reload); 
end
if nargin < 2
   CtrlParam.use = 1;
   CtrlParam.precomputed = 1;
   CtrlParam.MPC.use = 0;
   CtrlParam.MPC.Condensing = 1;
   CtrlParam.RBC.use = 0;
   CtrlParam.PID.use = 0;
end

% controller parameters
controller.use = CtrlParam.use;
controller.precomputed.use = CtrlParam.precomputed;
controller.MPC.use =    CtrlParam.MPC.use;
controller.MPC.Condensing =    CtrlParam.MPC.Condensing;
controller.RBC.use =    CtrlParam.RBC.use;
controller.PID.use =    CtrlParam.PID.use;

fprintf('\n------------------ Controller -----------------------\n');

if CtrlParam.precomputed      % precomputed inputs and outputs or real measurements
    fprintf('*** Load pre-computed controls ... \n')
    
    path = ['../buildings/', model.buildingType];  
%     if model.buildingType == 'Infrax'
%         load([path '/preComputed_matlab/INFRAX_inputs.mat']);     % load real inputs
%         load([path '/preComputed_matlab/INFRAX_outputs.mat']);     % load real inputs
%         U = INFRAXinputs_zero_shade';
%         Y = INFRAXoutputs';
%     elseif model.buildingType == 'Reno'
        load([path '/preComputed_matlab/preComputedControls.mat']);
%     end
    
controller.precomputed.U = U;  
controller.precomputed.Y = Y;    

    fprintf('*** Done.\n')
elseif CtrlParam.MPC.use  
    fprintf('*** Create controllers ... \n')
    
    %    CTRL DESIGN 
% RBC, MPC, PID, ML, etc
    
    fprintf('*** Done.\n')
else
    
    
end





end