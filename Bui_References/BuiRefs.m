function references = BuiRefs(model)

% TODO:  FIX THIS 
% TODO: Variable TZoneSetLow not found in Data/preComputedInfrax.mat

if nargin == 0
   model.buildingType = 'Reno';  
   model.pred.ny = 19;
%    addpath('../common_files_all_sims/Data')
   path = ['../buildings/', model.buildingType, '/disturbances/'];
end

fprintf('*** Load references ... \n')

%% comfort boundaries
% function eval for full year comfort boundaries profiles in K
[t_comf, TLow, TUp, TSup, TRefControl] = comfortTemperature(path);
% % visualisation of comfort constraints and reference
% plot(t_comf,[TLow, TUp, TLow+(TUp-TLow)/2])
% legend('TLow','TUp','ref')
% figure
% plot(t_comf,TSup)
% legend('supply watter')

fprintf('*** Done.\n')

% % width of the - dynamic comfort zone
references.ref = (TLow+(TUp-TLow)/2)*ones(1,model.pred.ny);  % setpoint in K
references.wb = TLow*ones(1,model.pred.ny); %  below threshold
references.wa = TUp*ones(1,model.pred.ny); %  above threshold


end