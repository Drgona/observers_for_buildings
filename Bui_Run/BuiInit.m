
%% BuiSim 
% Matlab toolbox for fast developlent, simulation and deployment of
% advanced building climate controllers

% TODO long term:
% API for end user
% communication with BMS integrate
% code generation integrate, so implementation is matlab free and matlab is
% used only during design phase


yalmip('clear');

addpath('../Bui_Modeling/')
addpath('../Bui_Disturbances/')
addpath('../Bui_References/')
addpath('../Bui_Estimation/')
addpath('../Bui_Control/')
addpath('../Bui_Simulation/')


%% MODEL   emulator + predictor
% available building types  'Infrax', 'Reno', 'Old', 'RenoLight'
% buildingType = 'Infrax';  ModelOrders.range = [100, 200, 600]; % orderds for INFRAX add any reduced order model you wish to have
buildingType = 'Borehole';   ModelOrders.range = [10, 15, 20, 40, 100];  % orderds for borehole 
% buildingType = 'RenoLight'; ModelOrders.range = [4, 7, 10, 15, 20, 30, 40, 100];  % orderds for residential house
% ModelOrders.choice = 15;   
ModelOrders.choice = 'full';
ModelOrders.off_free = 0;    %  augmented model
reload = 0; 

model = BuiModel(buildingType, ModelOrders, reload);



%% disturbacnes 
dist = BuiDist(buildingType, reload);


%% References 
% TODO:  fix to be universal for all models, add multiple options
% TODO: error with the path to common files - fix that
refs = BuiRefs();


%%  estimator 
EstimParam.LOPP.use = 0;      %  Luenberger observer via pole placement - Not implemented
EstimParam.SKF.use = 0;    % stationary KF
EstimParam.TVKF.use = 0;   % time varying KF
EstimParam.MHE.use = 1;   % moving horizon estimation via yalmip
EstimParam.MHE.Condensing = 0;   % state condensing 

% TODO: flag for using estimation, if not use states from plant model
EstimParam.use = 1;

estim = BuiEstim(model, EstimParam);

% TODO: tune TVKF for infrax, one output is begaving strange

%% controller 
CtrlParam.precomputed = 1; % TODO: error with the path to common files - fix that
CtrlParam.MPC.use = 0;
CtrlParam.MPC.Condensing = 1;
CtrlParam.RBC.use = 0;
CtrlParam.PID.use = 0;

% TODO: flag for controller use, if not perform open loop simulations 
CtrlParam.use = 1;

ctrl = BuiCtrl(model, CtrlParam);
ctrl.precomputed.U = ctrl.precomputed.U-13.5;  % initialization compensation for borehole - TODO fix this generalization
% TODO higher level tuning params
% PID coefficients
% MPC - just magnutide of weight on comfort and energy 


%% Simulate
sim.run.start = 1;
% sim.run.end = 8; 
sim.run.end = 200; 
sim.verbose = 1;
sim.flagSave = 0;

% TODO: modify plot option in BuiSim init
PlotParam.flagPlot = 1;     % plot 0 - no 1 - yes
PlotParam.plotStates = 0;        % plot states
PlotParam.plotDist = 0;        % plot disturbances
PlotParam.plotEstim = 1;        % plot disturbances
PlotParam.plotCtrl = 0;        % plot disturbances
PlotParam.paper = 1;        % paper format

% PlotParam.reduced = 0;   %  reduced paper plots formats 0 - no 1 - yes
% PlotParam.zone = 2;     % choose zone if reduced
% PlotParam.only_zone = 0;    %  plot only zone temperatures 0 - no 1 - yes  

% %  simulation file with embedded plotting file
outdata = BuiSim(model, estim, ctrl, dist, refs, sim, PlotParam);

% % recreate plots for paper
% BuiPlot(outdata,PlotParam)


% plots 7 days
start = 30; 
SimStop_sec = 7*86400;
finish = ceil(SimStop_sec/model.plant.Ts);
time = ((start:finish)*480/86400)';

% PAPER PLOTS - estimated states
dx0 = (model.pred.Fd - 273.15)*ones(model.pred.nx,1);
figure
subplot(2,1,1)
ribbon(time,outdata.data.Xe(:,start:finish)'+dx0')
shading flat
grid on
title('MHE estimated state trajectories 3D')
ylabel('time [days]')
zlabel('temperatures [^oC]')
xlabel('states [index]')
set(gca,'fontsize',22)
subplot(2,1,2)
plot(time,outdata.data.Xe(:,start:finish)'+dx0','linewidth', 2)
title('MHE estimated state trajectories 2D')
xlabel('time [days]')
ylabel('temperatures [^oC]')
grid on
set(gca,'fontsize',22)

% PAPER PLOTS - VALIDATION
% load('../buildings/Borehole/preComputed_matlab/preComputedValidation.mat')
% % ordering of validation states
% load('../buildings/Borehole/preComputed_matlab/Validation_order.mat')
% [temp, order] = sort(X_order(1,:));
% New_order = X_order(:,order);
% Validation = InfraxValidation(:,order);

load('../buildings/Borehole/preComputed_matlab/preComputedValidation_mFlowVar.mat')
Validation = nonlinearstatesmFlowVar;

XE1 = outdata.data.Xe(:,start:finish)'+dx0';
XS1 = Validation(start:finish,:)-273.15; 
DX1 = XS1-XE1;

figure
subplot(2,1,1)
ribbon(time,Validation(start:finish,:)-273.15)
shading flat
grid on
title('Simulated state trajectories 3D')
ylabel('time [days]')
zlabel('temperatures [^oC]')
xlabel('states [index]')
set(gca,'fontsize',22)
subplot(2,1,2)
plot(time,Validation(start:finish,:)-273.15,'linewidth', 2)
title('Simulated state trajectories 2D')
xlabel('time [days]')
ylabel('temperatures [^oC]')
grid on
set(gca,'fontsize',22)
% subplot(3,1,3)
% boxplot(DX1)
% title('Box-plot of state estimation errors')
% xlabel('states []')
% ylabel('temperatures [^oC]')
% grid on
% set(gca,'fontsize',22)
% set(gca,'xticklabel',{[]})

% statistics full dataset 200 days
start2 = 30; 
finish2 = length(outdata.data.Xe(:,start2:end));
XE = outdata.data.Xe(:,start2:finish2)'+dx0';
XS = Validation(start2:finish2,:)-273.15; 
DX = XS-XE;

figure
subplot(2,1,1)
plot(DX)
title('State estimation Errors')
ylabel('temperatures [^oC]')
xlabel('time [days]')
set(gca,'fontsize',22)
subplot(2,1,2)
boxplot(DX)
xlabel('states []')
ylabel('temperatures [^oC]')
grid on
set(gca,'fontsize',22)
set(gca,'xticklabel',{[]})

% maximum error
MAX_DX = max(max(abs(DX)))
% mean error
MEAN_DX = mean(mean(DX))
% IAE/simtime/nrof states
IAE_DX = sum(sum(abs(DX)))/length(start:finish)/model.plant.nx





return


% figure
% waterfall(outdata.data.Xe(:,start:finish)'+dx0')

% % ESTIMATED STATES
dx0 = (model.pred.Fd - 273.15)*ones(model.pred.nx,1);
% estimated states 
figure
subplot(2,2,1)
surf(outdata.data.Xe(:,start:finish)'+dx0')
shading flat
title('X surface')
ylabel('time [steps]')
zlabel('temperatures [^oC]')
xlabel('states [index]')
subplot(2,2,2)
ribbon(outdata.data.Xe(:,start:finish)'+dx0')
shading flat
title('X ribbon')
ylabel('time [days]')
zlabel('temperatures [^oC]')
xlabel('states [index]')
subplot(2,2,3)
imagesc(outdata.data.Xe(:,start:finish)+dx0)
% contourf(outdata.data.X',30,'LineColor','none')
title('X heatmap')
xlabel('time [steps]')
ylabel('states [index]')
subplot(2,2,4)
plot(outdata.data.Xe(:,start:finish)'+dx0')
title('X trajectories')
xlabel('time [steps]')
ylabel('temperatures [^oC]')
grid on

% finish = outdata.sim.run.Nsim;

% % NON-LINEAR MODEL SIMULATED STATES
% load('../Data/borehole/AnalyticalStateTraj.mat')
load('../buildings/Borehole/preComputed_matlab/preComputedValidation.mat')
figure
subplot(2,2,1)
surf(InfraxValidation(start:finish,:)-273.15)
shading flat
title('X surface')
ylabel('time [steps]')
zlabel('temperatures [^oC]')
xlabel('states [index]')
subplot(2,2,2)
ribbon(InfraxValidation(start:finish,:)-273.15)
shading flat
title('X ribbon')
ylabel('time [steps]')
zlabel('temperatures [^oC]')
xlabel('states [index]')
subplot(2,2,3)
imagesc(InfraxValidation(start:finish,:)-273.15)
% contourf(outdata.data.X',30,'LineColor','none')
title('X heatmap')
ylabel('time [steps]')
xlabel('states [index]')
colorbar
subplot(2,2,4)
plot(InfraxValidation(start:finish,:)-273.15)
title('X trajectories')
xlabel('time [steps]')
ylabel('temperatures [^oC]')
grid on



return

% cutting the first datapoints
outdata.sim.run.Nsim = outdata.sim.run.Nsim -29;
outdata.data.X = outdata.data.X(:,30:end);    % Load here simulated states
outdata.data.U = outdata.data.U(:,30:end);
outdata.data.Y = outdata.data.Y(:,30:end);
outdata.data.Xe = outdata.data.Xe(:,30:end);
outdata.data.Ye = outdata.data.Ye(:,30:end);
outdata.data.Xp = outdata.data.Xp(:,30:end);
outdata.data.Yp = outdata.data.Yp(:,30:end);


BuiPlot(outdata,PlotParam)

figure
subplot(2,2,1)
imagesc(model.pred.Ad)
colorbar
title('A')
subplot(2,2,2)
imagesc(model.pred.Bd)
colorbar
title('B')
subplot(2,2,3)
imagesc(model.pred.Cd)
colorbar
title('C')
subplot(2,2,4)
imagesc(model.pred.Gd)
colorbar
title('Gd')

figure
CA = model.pred.Cd'.*model.pred.Ad;
imagesc(CA((model.pred.Cd ~= 0),:))
colorbar
title('CA')

load('C:\Users\u0107194\Documents\Code_reposit\Playground\Observers_working_paper\buildings\Borehole\models\ssm.mat')
figure
subplot(2,2,1)
imagesc(A)
colorbar
title('Ac')
subplot(2,2,2)
imagesc(B)
colorbar
title('Bc')
subplot(2,2,3)
imagesc(C)
colorbar
title('Cc')
subplot(2,2,4)
imagesc(D)
colorbar
title('Dc')



dx0 = (model.pred.Fd - 273.15)*ones(model.pred.nx,1);
% estimated states 
figure
subplot(2,2,1)
surf(outdata.data.Xe(:,30:end)'+dx0')
shading flat
title('X surface')
ylabel('time [steps]')
zlabel('temperatures [^oC]')
xlabel('states [index]')
subplot(2,2,2)
ribbon(outdata.data.Xe(:,30:end)'+dx0')
shading flat
title('X ribbon')
ylabel('time [steps]')
zlabel('temperatures [^oC]')
xlabel('states [index]')
subplot(2,2,3)
imagesc(outdata.data.Xe(:,30:end)+dx0)
% contourf(outdata.data.X',30,'LineColor','none')
title('X heatmap')
ylabel('time [steps]')
xlabel('states [index]')
subplot(2,2,4)
plot(outdata.data.Xe(:,30:end)'+dx0')
title('X trajectories')
xlabel('time [steps]')
ylabel('temperatures [^oC]')

load('../Data/borehole/LinearStateTraj2weeks.mat')
figure
subplot(2,2,1)
surf(LinearStates(30:end,:)-273.15)
shading flat
title('X surface')
ylabel('time [steps]')
zlabel('temperatures [^oC]')
xlabel('states [index]')
subplot(2,2,2)
ribbon(LinearStates(30:end,:)-273.15)
shading flat
title('X ribbon')
ylabel('time [steps]')
zlabel('temperatures [^oC]')
xlabel('states [index]')
subplot(2,2,3)
imagesc(LinearStates(30:end,:)-273.15)
% contourf(outdata.data.X',30,'LineColor','none')
title('X heatmap')
ylabel('time [steps]')
xlabel('states [index]')
colorbar
subplot(2,2,4)
plot(LinearStates(30:end,:)-273.15)
title('X trajectories')
xlabel('time [steps]')
ylabel('temperatures [^oC]')

load('../Data/borehole/SimStateTraj2weeks.mat')
figure
subplot(2,2,1)
surf(SimulationStates(30:end,:)-273.15)
shading flat
title('X surface')
ylabel('time [steps]')
zlabel('temperatures [^oC]')
xlabel('states [index]')
subplot(2,2,2)
ribbon(SimulationStates(30:end,:)-273.15)
shading flat
title('X ribbon')
ylabel('time [steps]')
zlabel('temperatures [^oC]')
xlabel('states [index]')
subplot(2,2,3)
imagesc(SimulationStates(30:end,:)-273.15)
% contourf(outdata.data.X',30,'LineColor','none')
title('X heatmap')
ylabel('time [steps]')
xlabel('states [index]')
colorbar
subplot(2,2,4)
plot(SimulationStates(30:end,:)-273.15)
title('X trajectories')
xlabel('time [steps]')
ylabel('temperatures [^oC]')


return

figure
subplot(2,2,1)
surf(outdata.data.Xe(:,30:end)')
shading flat
title('X surface')
subplot(2,2,2)
ribbon(outdata.data.Xe(:,30:end)')
shading flat
title('X ribbon')
subplot(2,2,3)
imagesc(outdata.data.Xe(:,30:end))
% contourf(outdata.data.X',30,'LineColor','none')
title('X heatmap')
colorbar
subplot(2,2,4)
plot(outdata.data.Xe(:,30:end)')
title('X trajectories')



% figure
% plot(outdata.data.Ye(:,30:end)')
% title('Y trajectories')
% hold on
% plot((model.pred.Cd*outdata.data.Xe(:,30:end)+model.pred.Fd)','--')


% estimated states - Gd
figure
subplot(2,2,1)
surf(outdata.data.Xe(:,30:end)'-model.pred.Gd')
shading flat
title('X surface')
subplot(2,2,2)
ribbon(outdata.data.Xe(:,30:end)'-model.pred.Gd')
shading flat
title('X ribbon')
subplot(2,2,3)
imagesc(outdata.data.Xe(:,30:end)-model.pred.Gd)
% contourf(outdata.data.X',30,'LineColor','none')
title('X heatmap')
colorbar
subplot(2,2,4)
plot(outdata.data.Xe(:,30:end)'-model.pred.Gd')
title('X trajectories')




return

% figure
% plot(outdata.data.Xe(:,100:end)')
% figure
% figure; imagesc(model.pred.Ad.*outdata.data.Xe(:,120)')


Nsim = 1000;
x10 = 11*ones(model.pred.nx,1);
x20 = 11*ones(model.pred.nx,1);
xk1 = zeros(model.pred.nx,Nsim);
xk2 = zeros(model.pred.nx,Nsim);
yk = zeros(model.pred.ny,Nsim);
uk = ctrl.precomputed.U(:,1:Nsim);

figure
    subplot(2,1,1)
    imagesc(model.pred.Ad)
    colorbar
%     subplot(3,1,2)
%     imagesc(model.pred.Bd)
%     colorbar
    subplot(2,1,2)
    plot(xk2')
%     subplot(2,2,4)
    plot(uk')
for i = 1:Nsim
    
    xk1(:,i) = model.pred.Ad*x10 + model.pred.Bd*uk(:,i);
    xk2(:,i) = sum(model.pred.Ad.*x20',2) + model.pred.Bd*uk(:,i);
    yk(:,i) =  model.pred.Cd*x20;
    
    x10 = xk1(:,i);
    x20 = xk2(:,i);
    
    pause(0.001)
    subplot(2,1,1)
    imagesc(model.pred.Ad.*x20')
    colorbar
%     caxis([0 12])
%     subplot(3,1,2)
%     imagesc(model.pred.Bd.*uk(:,i)')
%     colorbar
%     caxis([0 12])
    
    subplot(2,1,2)
    plot(xk2')
%     subplot(2,2,4)
%     plot(uk')
end


% discard diagonal
out = model.pred.Ad - diag(diag(model.pred.Ad));
figure;     imagesc(out)

% figure
% plot(xk1')
% figure
% plot(xk2')
% 
% A = ones(3)
% a = [1 2 3]'
% A*a
% sum(A.*a',2)


