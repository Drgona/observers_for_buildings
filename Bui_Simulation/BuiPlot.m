function BuiPlot(outdata,PlotParam)

%% Description
% plot_data.m - general file for plotting the results from run files

if nargin < 2
   PlotParam.plotStates = 0;        % plot states
   PlotParam.plotDist = 0;        % plot disturbances
   PlotParam.plotEstim = 0;        % plot disturbances
   PlotParam.plotCtrl = 0;        % plot disturbances
   PlotParam.paper = 1;        % paper format
end

% init
Nsim = outdata.sim.run.Nsim; 
Time = (1:Nsim)*outdata.model.plant.Ts/3600/24;  % days

  %% STATES   - TODO: categorize states based on zones
  
% initial sate conditions in K
x_init = outdata.model.plant.Fd(1);
% x_init = model.plant.x0;
  
if PlotParam.plotStates
    figure
    plot(Time, outdata.data.X(:,1:end-1)+x_init, 'linewidth', 2);
    title('States');
    axis tight
    grid on
    ylabel('Temp [K]')
    xlabel('time [days]')
end

 %% DISTURBANCES - TODO: categorize disturbances based on magnitudes
if PlotParam.plotDist
    figure
    plot(Time, outdata.data.D, 'linewidth', 2);
    datetick
    axis tight
    title('Disturbances');
    grid on
    ylabel('[-]')
    xlabel('time [days]')
end


%%   ESTIMATOR

% if not(PlotParam.only_zone)
if outdata.estim.use && PlotParam.plotEstim
 
    if PlotParam.paper
             % outputs - simulated vs estimated
        figure('DefaultAxesFontSize',16)
        subplot(3,1,1)
        title('Simulated vs estimated outputs');
        hold on
        plot(Time, outdata.data.Y-273.15,'-' ,'linewidth', 2);
        plot(Time, outdata.data.Ye-273.15, '--', 'linewidth', 2);
        axis tight
        grid on
        ylabel('T [^{\circ}C]')
%         legend('simulated','estimated')
%         xlabel('time [days]')   
          
        % output estimation error
        subplot(3,1,2)
        plot(Time,(outdata.data.Y-outdata.data.Ye),'linewidth', 2)
        title('Output estimation error')
        axis tight
        grid on
        ylabel('ey [^{\circ}C]')
%         xlabel('time [days]')  
   
        % statesestimated 
        subplot(3, 1, 3);
        plot(Time, outdata.data.Xe(:,1:end)+x_init-273.15,'linewidth', 2);
        title('Estimated states');
        axis tight
        grid on
        xlabel('time [days]')
        ylabel('T [^{\circ}C]')
        
        figure('DefaultAxesFontSize',16)
        boxplot(outdata.data.EstimError')
        title('Box plot output estimation error');
        axis tight
        grid on
        xlabel('zone')
        ylabel('ey [^{\circ}C]')
        
    else
        % outputs - simulated vs estimated
        figure
        subplot(2,1,1)
        title('Simulated vs estimated outputs');
        hold on
        plot(Time, outdata.data.Y-273.15,'-' ,'linewidth', 2);
        plot(Time, outdata.data.Ye-273.15, '--', 'linewidth', 2);
        axis tight
        grid on
        ylabel('T [^{\circ}C]')
%         legend('simulated','estimated')
        xlabel('time [days]')   
          
        % output estimation error
        subplot(2,1,2)
        plot(Time,(outdata.data.Y-outdata.data.Ye),'linewidth', 2)
        title('Output estimation error')
        axis tight
        grid on
        ylabel('T [^{\circ}C]')
        xlabel('time [days]')  
   
        % states - simulated vs estimated 
        figure
        subplot(2, 1, 1);
        plot(Time, outdata.data.X(:,1:end-1)+x_init-273.15, 'linewidth', 2);
        title('states: simulated');
        axis tight
        grid on
        xlabel('time [days]')
        ylabel('Temp [^{\circ}C]')
        
        subplot(2, 1, 2);
        plot(Time, outdata.data.Xe(:,1:end)+x_init-273.15,'linewidth', 2);
        title('states:  estimated');
        axis tight
        grid on
        xlabel('time [days]')
        ylabel('T [^{\circ}C]')
    end
        
    if outdata.estim.MHE.use   % MHE variables 
        figure
        subplot(2,1,1)
        plot(Time, outdata.data.We,'linewidth', 2)
        title('We')
        grid on
        subplot(2,1,2)
        plot(Time, outdata.data.Ve,'linewidth', 2)
        title('Ve')
        grid on
%         subplot(3,1,3)
%         plot(Time(outdata.estim.MHE.N+1:end), outdata.data.Y(:,outdata.estim.MHE.N+1:end),'linewidth', 2)
%         hold on
%         plot(Time(outdata.estim.MHE.N+1:end), outdata.data.Yestim(:,outdata.estim.MHE.N+1:end),'--','linewidth', 2)
%         title('Yestim')
%         grid on

    end
        
end
% end
  

if outdata.ctrl.use && PlotParam.plotCtrl
    
     if outdata.ctrl.MPC.use  % mpc
     % --- objective function ---
        figure
        stairs(Time, outdata.data.J, 'linewidth', 2);
        hold on
        stairs(Time, outdata.data.J_v, '--','linewidth', 2);
        stairs(Time, outdata.data.J_u, '--','linewidth', 2);
        stairs(Time, outdata.data.OBJ, '--','linewidth', 2);
        legend('objective','obj. viol,','obj. inputs', 'yalmip obj.')
        title('objective value increments');
        axis tight
        grid on       
     end  
    
     
%      OUTPUTS
     figure
     subplot(2, 1, 1); 
     title('Indoor temperature','fontsize',24); 
     
     plot(Time, outdata.data.Y-273.15, 'linewidth', 2);
     axis tight
     grid on
     ylabel('Zone temperatures [\circC]','fontsize',22)  
     set(gca,'fontsize',22)
%      box on
% %         slight rotation to prevent misplotting
%      ax = gca;
%      ax.XTickLabelRotation=1; 

     if not(outdata.ctrl.precomputed.use)
         %      TODO: unify References for all controllers for plotting
         Rmin = outdata.data.wb;
         Rmax = outdata.data.wa;
         R = outdata.data.R;
  
         stairs(Time, Rmin-273.15, 'k--', 'linewidth', 2);
         stairs(Time, Rmax-273.15, 'k--', 'linewidth', 2);
     end



%      INPUTS
    subplot(2, 1, 2); 
    title('Heating','fontsize',24);   
    hold on
    h = stairs(Time, outdata.data.U');
    set(h, 'linewidth', 2, 'linestyle', '-');
    axis tight
    grid on
    set(gca,'fontsize',22)
    %           box on
% %         slight rotation to prevent misplotting
%           ax = gca;
%           ax.XTickLabelRotation=1;  

     if not(outdata.ctrl.precomputed.use)
         % control constraints - TODO unify for all controllers
          Umax = outdata.model.pid.umax*ones(1,Nsim+1);
          Umin = outdata.model.pid.umin*ones(1,Nsim+1);
          plot(Time, Umax, 'k--', Time, Umin, 'k--', 'linewidth', 2 );
          ylabel(['u [W]'],'fontsize',22)
          ylim([-Umin(1,1)-Umax(1,1)/10 , Umax(1,1)+Umax(1,1)/10])    
          xlabel('time [days]','fontsize',22)             
     end

end
  

end

