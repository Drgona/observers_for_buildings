function outdata = BuiSim(model, estim, ctrl, dist, refs, sim, PlotParam)

if nargin == 0  % model
   buildingType = 'Infrax';  
   ModelOrders.range = [100, 200, 600]; % add any reduced order model you wish to have
%    ModelOrders.choice = 200;            % insert model order or 'full' for full order SSM 
   ModelOrders.choice = 100;            % insert model order or 'full' for full order SSM 
   ModelOrders.off_free = 0;            %  augmented model
   reload = 0;
   model = BuiModel(buildingType, ModelOrders, reload);     %  construct the model
end
if nargin < 2   % estimator
   EstimParam.LOPP.use = 0;      %  Luenberger observer via pole placement 
   EstimParam.SKF.use = 0;    % stationary KF
   EstimParam.TVKF.use = 1;   % time varying KF
   EstimParam.MHE.use = 0;   % moving horizon estimation via yalmip
   EstimParam.MHE.Condensing = 1;   % moving horizon estimation via yalmip
   estim = BuiEstim(model, EstimParam);
end
if nargin < 3   % controller
   CtrlParam.precomputed = 1;
   CtrlParam.MPC.use = 0;
   CtrlParam.MPC.Condensing = 1;
   CtrlParam.RBC.use = 0;
   CtrlParam.PID.use = 0;
   ctrl = BuiCtrl(model, CtrlParam);
end
if nargin < 4  % disturbances
   dist = BuiDist(model.buildingType, model.reload);
end
if nargin < 5   % references
   refs = BuiRefs();      % TODO FIX to be general
end
if nargin < 6   % simulation parameters
    sim.run.start = 1;     % starting day
%     sim.run.end = 13;       % finishing day
    sim.run.end = 3;       % finishing day
    sim.verbose = 1;
    sim.flagSave = 0;
end
if nargin < 7   % plotting  
    PlotParam.flagPlot = 0;  % plot 0 - no 1 - yes
    PlotParam.reduced = 0;   % simplified paper plots formats 0 - no 1 - yes
    PlotParam.zone = 2;    % choose zone if simplified
    PlotParam.only_zone = 0;    %  plot only zone temperatures 0 - no 1 - yes     
end


%% Simulation setup   
fprintf('\n------------------ Simulation Setup -----------------\n');

fprintf('*** Building Type = %s\n' , model.buildingType);
fprintf('*** Prediction model order = %d, \n',   size(model.pred.Ad,1))
fprintf('*** Start day = %d , End day = %d \n', sim.run.start, sim.run.end);


%% Simulation steps   
% starting and finishing  second:  24h = 86400 sec
SimStart_sec = (sim.run.start-1)*86400;
SimStop_sec = (sim.run.end)*86400;
% starting and finishing  step for simulation loop - MPC
SimStart = floor(SimStart_sec/model.plant.Ts)+1;
SimStop = ceil(SimStop_sec/model.plant.Ts);
% number of simulation steps for MPC
Nsim = length(SimStart:SimStop);

%% Initial values  

X = zeros(model.plant.nx,Nsim+1);

if ctrl.precomputed.use   % precomputed inputs and outputs
    U = ctrl.precomputed.U(:,SimStart:SimStop);
    Y = ctrl.precomputed.Y(:,SimStart:SimStop)+273.15;
    D = dist.d(SimStart:SimStop,:)';
else    % initialize matrices for closed loop control simulations
    Y = zeros(model.plant.ny,Nsim);
    U = zeros(model.plant.nu,Nsim);
    D = zeros(model.plant.nd,Nsim);
end
      
if estim.use ==1
    %     estmator vector inits
    Xp = zeros(model.pred.nx,Nsim+1);
    Xe = zeros(model.pred.nx,Nsim);
    Ye = 295.15*ones(model.pred.ny,Nsim);
    Yp = 295.15*ones(model.pred.ny,Nsim);
    % estimation error
    EstimError = zeros(model.pred.ny,Nsim);
    
    % current estim states
    xe = Xe(:, 1);  
    xp = Xp(:, 1); 
    ye = Ye(:, 1);  
    yp = Yp(:, 1); 
    
    if estim.TVKF.use
        EstimGain = cell(1,Nsim);
        ErrorCovar = cell(1,Nsim); 
    end

    if estim.MHE.use
        OBJ_MHE = zeros(1,Nsim);
        We = zeros(model.pred.nx,Nsim);
        Ve = zeros(model.pred.ny,Nsim);
       
    end   
end
    



%% ------ MAIN simulation loop ------
    % % ------ Verbose ------
fprintf('\n---------------- Simulation Running -----------------');
% % initiation clearing string 
reverseStr = '';

start_t = clock;

for k = 1:Nsim
    
%     current inputs, outputs and disturnances
    uopt = U(:,k); 
    yn = Y(:,k); 
    d0 = D(:,k);  
         
        if estim.SKF.use  % stationary KF
            
            % measurement update                              
            yp = model.pred.Cd*xp + model.pred.Dd*uopt + model.pred.Fd*1;          % output estimation
            ep = yn - yp;                                                       % estimation error
            xe = xp  + estim.SKF.L1*ep;                                       % estimated state
            
            % time update
            xp = model.pred.Ad*xe + model.pred.Bd*uopt + model.pred.Ed*d0 + model.pred.Gd*1;
            
            ye = model.pred.Cd*xe + model.pred.Dd*uopt + model.pred.Fd*1;     % output estimate with x[n|n]
             
        elseif estim.TVKF.use  % time varying KF           
            
              if k == 1
                  P = model.pred.Bd*estim.TVKF.Qe*model.pred.Bd';         % Initial error covariance   
%                   P = estim.TVKF.Qe*model.pred.Bd*model.pred.Bd';         % Initial error covariance   
              end
            
              % Measurement update
              L1 = P*model.pred.Cd'/(model.pred.Cd*P*model.pred.Cd'+estim.TVKF.Re); % observer gain
              yp = model.pred.Cd*xp + model.pred.Dd*uopt + model.pred.Fd*1;          % output estimation
              ep = yn - yp;                                                       % estimation error
              xe = xp + L1*ep;                                                    % x[n|n]
              P = (eye(model.pred.nx)-L1*model.pred.Cd)*P;                          % P[n|n]   estimation error covariance
              errcov = model.pred.Cd*P*model.pred.Cd';                              % output estimation error covariance
              
              % Time update
              xp = model.pred.Ad*xe + model.pred.Bd*uopt + model.pred.Ed*d0 + model.pred.Gd*1;        % x[n+1|n]
              P = model.pred.Ad*P*model.pred.Ad' + model.pred.Bd*estim.TVKF.Qe*model.pred.Bd';       % P[n+1|n]
%               P = model.pred.Ad*P*model.pred.Ad' + estim.TVKF.Qe*model.pred.Bd*model.pred.Bd';       % P[n+1|n]
            
              ye = model.pred.Cd*xe + model.pred.Dd*uopt + model.pred.Fd*1;     % output estimate with x[n|n]
              
              % time varying parameters data
              EstimGain{k} = L1;
              ErrorCovar{k} = errcov;
              
        elseif estim.MHE.use  % TODO: moving horizon estimation
   
            if k >= estim.MHE.N
             
                N = estim.MHE.N;
                        
                if model.pred.nd == 0  % no disturbances
                    [opt_out, feasible, info1, info2] = estim.MHE.optimizer{{Y(:,k-N+1:k), U(:,k-N+1:k), Xp(:,k-N+1)}}; % optimizer with estimated states
                else % with disturbances
                    [opt_out, feasible, info1, info2] = estim.MHE.optimizer{{Y(:,k-N+1:k), U(:,k-N+1:k), D(:,k-N+1:k), Xp(:,k-N+1)}}; % optimizer with estimated states
                end
                
               
                xe = opt_out{1};    % estimated state at x[n-N+1|n]
                ve =  opt_out{2};   % v decision variables at  x[n-N+1:n|n]
                obj_estim =  opt_out{3}; 
                
                if estim.MHE.Condensing                                       
                    we = zeros(model.pred.nx,estim.MHE.N);
                else
                    we =  opt_out{4};   % w decision variables at  x[n-N+1:n|n]
                end                          
                
            %    MHE post processing, integration of states at x[n-N+1|n] via state
            %    update and w to get x[n|n]           
                for j = 1:N
                    xe = model.pred.Ad*xe + model.pred.Bd*U(:,k-N+j) + model.pred.Ed*D(:,k-N+j) + model.pred.Gd*1 + we(:,j);
                end
             
                ye = model.pred.Cd*xe + model.pred.Dd*uopt + model.pred.Fd*1 + ve(:,N);     % output estimate with x[n|n]               
                
                OBJ_MHE(:,k) = obj_estim;
                We(:,k) = we(:,N);
                Ve(:,k) = ve(:,N);
            else
%              TODO:   put KF or growing horizon implementation for initial
%              estimate
                                                      
            end     
        end

    %     estimator data
        Xp(:,k+1) = xp;
        Xe(:,k) = xe;
        Ye(:,k) = ye;
        Yp(:,k) = yp;
        
%         estimation error
        EstimError(:,k) = Y(:,k) - Ye(:,k);
        
            
%     REMAINING simulation time computation
    step_time = etime(clock, start_t);                  %  elapsed time of one sim. step
    av_step_time = step_time/k;                         % average_step_time
    rem_sim_time = av_step_time*(Nsim-k);           % remaining_sim_time
     
    msg = sprintf('\n*** estimated remaining simulation time = %.2f sec \n',rem_sim_time);    % statement   
    fprintf([reverseStr, msg]);                                                 % print statement
    reverseStr = repmat(sprintf('\b'), 1, length(msg));                         % clear line   
        
end



%% ---------- Output Data + verbose ------------

% TODO: modify to be general


% ------------ DATA -----------------

% SIMILATION STRUCTURES
outdata.model = model;    %  model 
outdata.estim = estim;    % estimator
outdata.ctrl = ctrl;      %  controller
% outdata.dist = dist;      %  disturbances
% outdata.dist = refs;      %  references
outdata.sim = sim;        %  simulation parameters
outdata.sim.run.Nsim = Nsim;    % sim steps

% plant simulation data 
outdata.data.X = X;         %  state vector
outdata.data.Y = Y;         %  output vector
outdata.data.U = U;         %  input vector
outdata.data.D = D;         %  disturbance vector

if ctrl.MPC.use
    outdata.data.dist = dist_mpc;  %  disturbances with preview 
end

% estimator data
if estim.use ==1
    outdata.data.Xe = Xe;         %  estimated state vector  [n|n]
    outdata.data.Ye = Ye;         %  estimated output vector [n|n]
    outdata.data.Xp = Xp;         %  previous estimaror state vector [n|n-1]
    outdata.data.Yp = Yp;         %  estimated output vector [n|n-1]
    
    outdata.data.EstimError = EstimError;
    outdata.data.EstimIAE = sum(sum(abs(EstimError)));
    outdata.data.EstimISE = sum(sum(EstimError.^2));

    if estim.TVKF.use
        outdata.data.EstimGain = EstimGain;
        outdata.data.ErrorCovar = ErrorCovar;
        
    elseif estim.MHE.use
        outdata.data.We = We;         
        outdata.data.Ve = Ve;    
    end

end



% % TODO: integrate this
% if not(ctrl.CtrlParam.precomputed)
% 
%     % referecne + comfort zone
%     % outdata.data.R = refs_mpc;  %  references vector
%     outdata.data.wa = wa;       %  above threshold
%     outdata.data.wb = wb;       %  below threshold
%     % PMV zone
%     outdata.data.PMVub = PMVub; %  above threshold
%     outdata.data.PMVlb = PMVlb; %  below threshold
%     %  comfot zone violations
%     outdata.data.AV = AboveViol;
%     outdata.data.BV = BelowViol;
%     outdata.data.V = Viol;
%     % PMV index profiles
%     outdata.data.PMV = PMV;
%     %  PMV zone violations
%     outdata.data.PMV_AV = PMVAboveViol;
%     outdata.data.PMV_BV = PMVBelowViol;
%     outdata.data.PMV_V = PMVViol;
%     
%     if ctrl.CtrlParam.MPC.use
%         % obj function
%         outdata.data.J = J;
%         outdata.data.J_v = J_v;
%         outdata.data.J_u = J_u;
%         outdata.data.OBJ = OBJ;
%     end
% end


%  elapsed time of simulation
outdata.info.cmp = etime(clock, start_t);


% print verbose condition
if sim.verbose
        % -------- PRINT --------------
        fprintf('\n------------------ Simulation Results ---------------\n');
        
    if not(ctrl.precomputed.use)
        %  energy cost
        fprintf('          Heating cost: %.2f kWh\n', outdata.info.OverallHeatingCost);
        fprintf('          Cooling cost: %.2f kWh\n', outdata.info.OverallCoolingCost);
        fprintf('            Total cost: %.2f kWh\n', outdata.info.OverallTotalCost);
        fprintf('               Comfort: %.2f Kh\n',  outdata.info.Overall_Kh);
        fprintf('        PMV violations: %.2f \n', outdata.info.Overall_PMV_TVS);
    end
    
        % compuation time
        fprintf('*** Simulation Time: %.1f secs\n', outdata.info.cmp);
    
end


%% Simulation ends  
outdata.info.date =  datestr(now);
fprintf('*** Simulation finished at: %s \n',  outdata.info.date);


%% save data - ADJUST THIS
if sim.flagSave
    str = sprintf('../Data/outData%s_from%d_to%d.mat', model.buildingType, sim.run.start, sim.run.end);
    save(str,'outdata');
end


%% plotting the outut data - TODO adapt
if PlotParam.flagPlot
    BuiPlot(outdata,PlotParam)
end



end