# README #

### What is this repository for? ###

Matlab simulation framework for evaluation and comparisson of state observers for buildings 

### How do I get set up? ###

run BuiInit.m 

### Main functions used ###
 * BuiModel.m 		 - construction of the building model 
 * BuiDist.m         - generating disturbances for chosen model
 * BuiRefs.m         - generating referebce signal for controllers
 * BuiEstim.m        - construction of the estimator
 * BuiCtrl.m         - construction of the controller - CURRENTLY NOT WORKING
 * BuiSim.m          - simulation file for estimation and control of building model with given references and disturbances


### Auxiliary functions ###
 
 * BuiPlot.m 		         - common file for plotting the results
 * BuiMHEdesign.m 		     - MHE formulation and construction of yalmip optimizer
 * comfortTemperature.m 	 - function eval for full year comfort boundaries profiles in K
 * disturbance.m		     - function generating disturbance profiles - currently works for Infrax building
 * disturbance_old.m	     - function generating disturbance profiles - currently works for residential building
 * findOutput		         - Finds an output from a Dymola outputfile, used in comfortTemperature.m and disturbance.m functions
 * findMultipleOutputs	     - Finds multiple outputs from a Dymola outputfile, used in comfortTemperature.m and disturbance.m functions
 * fGenerateSysAndRom	     - function generating full order and reduced order models for simulations

### Toolbox structure and standards ###
* In order to keep the functions for the creation/loading of the building model and disturbances working the standardized structure of the folder and names of the files with building data needs to be preserved.
* All the data for each building are stored in ./buildings/NameOfTheBuilding folder.
* The disturbance file needs to be stored as preComputed.mat in  ./buildings/NameOfTheBuilding/disturbances.
* The file BuiDist will process the preComputed.mat file and generate dis.mat file and store it in  ./buildings/NameOfTheBuilding/preComputed_matlab.
* The model needs to be stored as ssm.mat file in  ./buildings/NameOfTheBuilding/model.
* The function BuiModel will generate set of reduced order models and original model and store it in file mod.mat  in  ./buildings/NameOfTheBuilding/preComputed_matlab.
* The precomputed inputs and outputs needs to be stored as preComputedControls.mat in  ./buildings/NameOfTheBuilding/preComputed_matlab.
* To use the precomputed inputs and outputs, the indexes of input and disturbances variables (to split up the inputs) as well as indexes of the measured outputs (subset of the available outputs of the model) need to be stored in file indexing.mat  in  ./buildings/NameOfTheBuilding/preComputed_matlab.
